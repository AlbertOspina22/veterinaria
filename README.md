# CidenetVeterinariaOSB

## Descripción

En este proyecto se crea una integración ESB para una veterinaria. Está construida en el bus OSB, recibe comunicación mediente el protocolo SOAP y se conecta a una API que se encarga de realizar la logica del negocio. La integración permite realizar las operaciones para crear y consultar pacientes.


## Tecnologias utilizadas

- OSB 12c


## Infromación importante

### Datos de conexion con la integracion

Al desplegar la integración esta publicará el WSDL en la dirección: http://localhost:7101/veterinaria/v1?wsdl , es posible que cambie el puerto y la IP de acuerdo en el ambiete en que se despliegue.



