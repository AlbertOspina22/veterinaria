xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://veterinaria.osb.cidenet";
(:: import schema at "../../interfaces/wsdl/vetJava.wsdl" ::)

declare variable $input as element() (:: schema-element(ns1:ConsultaPacienteRq) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:ConsultaPacienteRq) ::)) as element() (:: schema-element(ns1:ConsultaPacienteRq) ::) {
    <ns1:ConsultaPacienteRq>
        {
            if ($input/ns1:CodigoChip)
            then <ns1:CodigoChip>{upper-case(data($input/ns1:CodigoChip))}</ns1:CodigoChip>
            else ()
        }
    </ns1:ConsultaPacienteRq>
};

local:func($input)
