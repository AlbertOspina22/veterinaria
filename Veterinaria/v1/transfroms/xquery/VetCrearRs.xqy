xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://veterinaria.osb.cidenet";
(:: import schema at "../../interfaces/xsd/vetOSB.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:CrearPacienteRs) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:CrearPacienteRs) ::)) as element() (:: schema-element(ns1:CrearPacienteRs) ::) {
    <ns1:CrearPacienteRs>
        <ns1:Descripcion>{fn:data($input/ns1:Descripcion)}</ns1:Descripcion>
        <ns1:Codigo>{fn:data($input/ns1:Codigo)}</ns1:Codigo>
    </ns1:CrearPacienteRs>
};

local:func($input)
