xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://veterinaria.osb.cidenet";
(:: import schema at "../../interfaces/xsd/vetOSB.xsd" ::)

declare variable $codigo as xs:string external;
declare variable $mensaje as xs:string external;

declare function local:func($codigo as xs:string, 
                            $mensaje as xs:string) 
                            as element() (:: schema-element(ns1:CrearPacienteRs) ::) {
    <ns1:CrearPacienteRs>
        <ns1:Descripcion>{fn:data($mensaje)}</ns1:Descripcion>
        <ns1:Codigo>{fn:data($codigo)}</ns1:Codigo>
    </ns1:CrearPacienteRs>
};

local:func($codigo, $mensaje)
