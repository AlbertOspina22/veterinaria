xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://veterinaria.osb.cidenet";
(:: import schema at "../../interfaces/wsdl/vetJava.wsdl" ::)

declare variable $input as element() (:: schema-element(ns1:CrearPacienteRq) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:CrearPacienteRq) ::)) as element() (:: schema-element(ns1:CrearPacienteRq) ::) {
    <ns1:CrearPacienteRq>
        <ns1:CodigoChip>{upper-case(data($input/ns1:CodigoChip))}</ns1:CodigoChip>
        <ns1:Nombre>{upper-case(data($input/ns1:Nombre))}</ns1:Nombre>
        <ns1:Especie>{upper-case(data($input/ns1:Especie))}</ns1:Especie>
        <ns1:SubEspecie>{upper-case(data($input/ns1:SubEspecie))}</ns1:SubEspecie>
        <ns1:Edad>{fn:data($input/ns1:Edad)}</ns1:Edad>
        <ns1:Observacion>{upper-case(data($input/ns1:Observacion))}</ns1:Observacion>
    </ns1:CrearPacienteRq>
};

local:func($input)
