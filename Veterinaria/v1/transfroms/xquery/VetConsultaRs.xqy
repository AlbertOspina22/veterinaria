xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://veterinaria.osb.cidenet";
(:: import schema at "../../interfaces/xsd/vetOSB.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:ConsultaPacienteRs) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:ConsultaPacienteRs) ::)) as element() (:: schema-element(ns1:ConsultaPacienteRs) ::) {
    <ns1:ConsultaPacienteRs>
        <ns1:Mensaje>{fn:data($input/ns1:Mensaje)}</ns1:Mensaje>
        <ns1:Codigo>{fn:data($input/ns1:Codigo)}</ns1:Codigo>
        {
            for $paciente in $input/ns1:Paciente
            return
                <ns1:Paciente>
                    <ns1:CodigoChip>{fn:data($paciente/ns1:CodigoChip)}</ns1:CodigoChip>
                    <ns1:Nombre>{fn:data($paciente/ns1:Nombre)}</ns1:Nombre>
                    <ns1:Especie>{fn:data($paciente/ns1:Especie)}</ns1:Especie>
                    <ns1:SubEspecie>{fn:data($paciente/ns1:SubEspecie)}</ns1:SubEspecie>
                    <ns1:Edad>{fn:data($paciente/ns1:Edad)}</ns1:Edad>
                    <ns1:Observacion>{fn:data($paciente/ns1:Observacion)}</ns1:Observacion>
                </ns1:Paciente>
        }
    </ns1:ConsultaPacienteRs>
};

local:func($input)
