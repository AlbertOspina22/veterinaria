xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://veterinaria.osb.cidenet";
(:: import schema at "../../interfaces/xsd/vetOSB.xsd" ::)

declare variable $mensaje as xs:string external;
declare variable $codigo as xs:string external;

declare function local:func($mensaje as xs:string, 
                            $codigo as xs:string) 
                            as element() (:: schema-element(ns1:ConsultaPacienteRs) ::) {
    <ns1:ConsultaPacienteRs>
        <ns1:Mensaje>{fn:data($mensaje)}</ns1:Mensaje>
        <ns1:Codigo>{fn:data($codigo)}</ns1:Codigo>
    </ns1:ConsultaPacienteRs>
};

local:func($mensaje, $codigo)
